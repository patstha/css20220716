import './style.css'
import typescriptLogo from './typescript.svg'
import { setupCounter } from './counter'
import { updatePageBackground } from './styler'

document.querySelector<HTMLDivElement>('#app')!.innerHTML = `
  <div>
    <a href="https://vitejs.dev" target="_blank">
      <img src="/vite.svg" class="logo" alt="Vite logo" />
    </a>
    <a href="https://www.typescriptlang.org/" target="_blank">
      <img src="${typescriptLogo}" class="logo vanilla" alt="TypeScript logo" />
    </a>
    <h1>Vite + TypeScript</h1>
    <div class="card">
      <button id="counter" type="button"></button>
      <button id="randomize" type="button">I am feeling lucky</button>
    </div>
    <p class="read-the-docs">
      Click I am feeling lucky to change the background of this page.
    </p>
  </div>
`

setupCounter(document.querySelector<HTMLButtonElement>('#counter')!)
updatePageBackground(document.querySelector<HTMLButtonElement>('#randomize')!)
